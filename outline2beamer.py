#!/usr/bin/env python2.7

# copying from outline.txt(basically "markdown" format) and pasting
# and formatting into Beamer markup is tedious. This script tries
# to automate it at least to some extent.
# (NOTE: not quite perfect, but I'm bored already so...)
# (seems like a recent of Pandoc might have been a better idea but
#  it only just occured to me to Google about such a thing)

import argparse
import re

def parse_opts():
  parser = argparse.ArgumentParser(
        description='Outline to Beamer conversion',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument('outline', type=str, help='Outline doc')
  parser.add_argument('out_beamer', type=str, help='Output beamer markup goes into this file')
  return parser.parse_args()

def write_level(lvl, text, out):
  pfx = ' ' * lvl * 4
  out.write('%s%s' % (pfx, text))

def indent_to_level(indent):
  return indent / 4

def parse_item(indent, lines, start_idx, outf):
  #print 'enter parse_item'
  i = start_idx
  lvl = indent_to_level(indent)
  txt_line = re.sub('^\s*\*\s*', '', lines[i])
  txt_indent = len(lines[i]) - len(txt_line)
  outf.write(txt_line)
  i += 1
  while i < len(lines):
    cur_line = lines[i]
    if cur_line is None:
      return i
    new_indent = len(cur_line) - len(cur_line.lstrip())
    if new_indent < txt_indent:
      return i
    outf.write('\n%s%s' % (' ' * (txt_indent + len('\item {')), cur_line))
    i += 1
  #print 'exit parse_item'
  return min(len(lines), i)

def parse_item_list(indent, lines, start_idx, outf):
  #print 'enter parse_item_list'
  i = start_idx
  lvl = indent_to_level(indent)
  assert lvl <= 1
  while i < len(lines):
    cur_line = lines[i]
    if cur_line is None:
      i += 1
      continue
    next_indent = len(cur_line) - len(cur_line.lstrip())
    if next_indent < indent:
      return i
    elif re.match('^\s*\*', lines[i]):
      if next_indent > indent:
        write_level(lvl+1, '\\begin{itemize}\n', outf)
        i = parse_item_list(next_indent, lines, i, outf)
        write_level(lvl+1, '\\end{itemize}\n', outf)
      else:
        write_level(lvl+1, '\\item {', outf) 
        i = parse_item(indent, lines, i, outf)
        outf.write('}\n')
    elif next_indent == indent:
      return i
  #print 'exit parse_item_list'
  return min(len(lines) - 1, i)

def parse_text(lines, start_idx, outf):
  i = start_idx
  #print 'line:', lines[i]
  while i < len(lines):
    if lines[i] is None or not re.match('^\s*\*', lines[i]):
      #print 'tadaa'
      if lines[i] is not None:
        outf.write(lines[i] + '\n')
      i += 1
    else:
      return i
  #print 'exit parse_text'
  return min(len(lines)-1, i)

def parse_slide(lines, outf):
  i = 0
  #print lines
  while i < len(lines):
    cur_line = lines[i]
    if cur_line is None:
      i += 1
    elif re.match('^\s*\*', lines[i]):
      indent = len(cur_line) - len(cur_line.lstrip())
      write_level(0, '\\begin{itemize}\n', outf)
      i = parse_item_list(indent, lines, i, outf)
      write_level(0, '\\end{itemize}\n', outf)
    else:
      i = parse_text(lines, i, outf)

def write_beamer(slides, out_file):
  with open(out_file, 'w') as out:
    for slide in slides:
      write_level(0, '\\begin{frame}{}\n', out)
      parse_slide(slide, out)
      write_level(0, '\\end{frame}\n\n', out)
      

if __name__ == '__main__':
  opts = parse_opts()
  all_slides = list() # list of lists, where each nested list contains the non-empty lines of a slide
  with open(opts.outline, 'r') as src:
    current_slide = list() # all the lines for one slide
    for line in src:
      line = line.rstrip()
      if len(line) == 0:
        if len(current_slide) > 0: # ignore empty lines at the start
          if current_slide[-1] is None:
            continue # collapse multiple empty lines into a single(None) marker
          else:
            current_slide.append(None)
      elif re.match('^===\s*Slide', line):
        if len(current_slide) > 0:
          all_slides.append(current_slide)
          current_slide = list()
      else:
        current_slide.append(line)
    if len(current_slide) > 0: # process the last slide
      all_slides.append(current_slide)
  write_beamer(all_slides, opts.out_beamer)
