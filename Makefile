SHELL=/bin/bash

LATEX=pdflatex
LATEXMK=latexmk
LATEXOPT=-file-line-error

MAIN=main
TEXFILES=lattice_image.tex dnn_image.tex

SOURCES=$(MAIN).tex Makefile bib/lite.bib $(TEXFILES)
FIGURES := $(shell ls figures/*.pdf)

all:    $(MAIN).pdf
	xdg-open $(MAIN).pdf

.refresh:
	touch .refresh

$(MAIN).pdf: $(MAIN).tex .refresh $(SOURCES) $(FIGURES) 
	$(LATEXMK) -pdf $(MAIN).tex

force:
	touch .refresh
	$(MAKE) $(MAIN).pdf

.PHONY: clean force all

clean:
	$(LATEXMK) -C $(MAIN).tex
	rm -f $(MAIN).pdfsync
	rm -rf *~ *.tmp
